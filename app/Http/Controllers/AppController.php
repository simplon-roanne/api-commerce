<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class AppController extends Controller
{
    protected $secretToken = 'mJxTXVXMfRzLg6ZdhUhM4F6Eutcm1ZiPk4fNmvBMxyNR4ciRsc8v0hOmlzA0vTaX';

    /**
     * Saves an order submitted via POST request.
     *
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function saveOrder(Request $request)
    {
        if($failedResponse = $this->failBearerTokenCheck($request)) {
            return $failedResponse;
        }
        if($failedResponse = $this->failUserAgentCheck($request)) {
            return $failedResponse;
        }

        $jsonParams = $request->json()->all();

        $requiredParams = [
            'order.product',
            'order.payment_method',
            'order.status',
            'order.client.email',
            'order.client.firstname',
            'order.client.lastname',
            'order.addresses.billing.address_line1',
            'order.addresses.billing.city',
            'order.addresses.billing.zipcode',
            'order.addresses.billing.country',
            'order.addresses.billing.phone',
            'order.addresses.shipping.address_line1',
            'order.addresses.shipping.city',
            'order.addresses.shipping.zipcode',
            'order.addresses.shipping.country',
            'order.addresses.shipping.phone',
        ];

        $flattenRequest = Arr::dot($jsonParams);

        foreach($requiredParams as $requiredParam) {

            if(!isset($flattenRequest[$requiredParam])) {
                return $this->responseJson400(["error" => "Request parameter '".$requiredParam."' not found"]);
            }
            if(!$flattenRequest[$requiredParam]) {
                return $this->responseJson400(["error" => "Request parameter '".$requiredParam."' is empty"]);
            }
        }

        if($failResponse = $this->failStatusCheck($flattenRequest['order.status'])) {
            return $failResponse;
        }

        $orderId = DB::table('orders')->insertGetId([
            'product' => $jsonParams['order']['product'],
            'payment_method' => $jsonParams['order']['payment_method'],
            'status' => $jsonParams['order']['status'],
            'client' => json_encode($jsonParams['order']['client']),
            'addresses_billing' => json_encode($jsonParams['order']['addresses']['billing']),
            'addresses_shipping' => json_encode($jsonParams['order']['addresses']['shipping']),
            'user_agent' => $request->server('HTTP_USER_AGENT'),
            'ip' => $request->ip(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        return response()->json([
            "success" => "Order successfully saved",
            "order_id" => $orderId
        ]);
    }

    /**
     * Updates the order status based on its id
     *
     * @param Request $request
     * @param string $orderId
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function updateStatus(Request $request, $orderId)
    {
        if($failedResponse = $this->failBearerTokenCheck($request)) {
            return $failedResponse;
        }
        if($failedResponse = $this->failUserAgentCheck($request)) {
            return $failedResponse;
        }

        if(!$order = DB::table('orders')->find($orderId)) {
            return $this->responseJson400(["error" => "Order id $orderId not found"]);
        }
        if($failResponse = $this->failStatusCheck($request->get('status'))) {
            return $failResponse;
        }

        DB::table('orders')->where('id', $orderId)->update([
            'status' => $request->get('status'),
        ]);

        return response()->json([
            "success" => "Order successfully saved",
            "order_id" => $orderId
        ]);
    }

    protected function failStatusCheck($statusValue)
    {
        if(!in_array($statusValue, ['WAITING', 'PAID'])) {
            return $this->responseJson400(["error" => "Request parameter order.status must be one of the following : ['WAITING', 'PAID']"]);
        }
        return false;
    }

    protected function failBearerTokenCheck(Request $request)
    {
        if(!$request->bearerToken()) {
            return $this->responseJson400(["error" => "Bearer token not found."]);
        }
        if($request->bearerToken() != $this->secretToken) {
            return $this->responseJson400(["error" => "Invalid Bearer token."]);
        }

        return false;
    }

    protected function failUserAgentCheck(Request $request)
    {
        if(!$request->server('HTTP_USER_AGENT')) {
            return $this->responseJson400(["error" => "User Agent is Required. Please use it to identify your application."]);
        }

        return false;
    }

    public function responseJson400($json)
    {
        return response()->json($json)->setStatusCode(400);
    }
}
