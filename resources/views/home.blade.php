<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>API Commerce</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="jumbotron">
        <h1 class="display-4">API Commerce : Documentation</h1>
        <p class="lead">Your personel bearer token has been generated for you</p>
        <hr class="my-4">
        <p>Use it to authorize every request through the API : <b>mJxTXVXMfRzLg6ZdhUhM4F6Eutcm1ZiPk4fNmvBMxyNR4ciRsc8v0hOmlzA0vTaX</b></p>
        <p class="lead">
            <a class="btn btn-primary btn-lg" href="https://app.swaggerhub.com/apis-docs/simplon-roanne/api-commerce/1.0.0-oas3" role="button" target="_blank">Read documentation</a>
        </p>
    </div>

</body>
</html>