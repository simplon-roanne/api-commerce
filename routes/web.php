<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->get('/', function () {
    return view('home');
});

$router->post('/order', 'AppController@saveOrder');
$router->post('/order/{id}/status', 'AppController@updateStatus');